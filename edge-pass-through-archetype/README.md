# API Archetype

This project, api-archetype, is the project API auto-generator component.

# Build Instructions 

## Prerequisites 
   - Maven 3.3 
   - Java 8 
### Commands 
```
mvn install 
```

# Usage

## COMMAND LINE USAGE as archetype

1. Open your File Directory and navigate to your project folder location where the new project is to be added
2. Make sure the api project you are about to create DOES NOT already exist in the current working directory, if so, delete it
3. Open a command prompt in this location. NOTE: In windows, simple type ‘cmd’ in the address bar and a command window will open in this location
4. If in doubt that Maven is installed type ‘mvn -v’ to view the version of Maven and Java you have installed and their locations on your system disks
5. If you do not see the information or you get mvn not found error, reinstall Maven and set your PATH variables according to the installation instructions
6. Be prepared with the following information:
    * artifactId: <Your Project Name> The name should be a all lowercase name with only alpha characters i.e. no dashes or underscores Example: memberapi
7. Enter the following Command:

   > mvn archetype:generate -DarchetypeGroupId=com.apigee.archetype -DarchetypeArtifactId=api-pass-through -DarchetypeVersion=1.0.0-SNAPSHOT     
   
8. When Prompted:
  * Enter groupId, package, artifactId and Team Name : All lowercase, no spaces, dashes or underscores 
  * Note: The groupId and package attributes are not currently used. set their values to changeme 
  * Hit return and the enter “y” for your acceptance of the settings you have made

# Example 
   
## UNIX  
```
   mvn -B archetype:generate -DarchetypeGroupId=com.apigee.archetype \
      -DgroupId=ignoreme -Dpackage=ignoreme -DarchetypeArtifactId=api-pass-through \
      -DarchetypeVersion=1.0.0-SNAPSHOT -DartifactId=helloWorld -DteamName=apollo 
```
  
## DOS 
  
```
  mvn archetype:generate -DarchetypeGroupId=com.apigee.archetype -DarchetypeArtifactId=api-pass-through -DarchetypeVersion=1.0.0-SNAPSHOT -DartifactId=helloWorld -DteamName=apollo -DinteractiveMode=false
```

The command will complete successfully and create the application in the folder that you ran this command.  

TESTING THE API:
-------------------------
Change into the project folder and follow the instructions in the README.md file 

