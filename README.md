# Build Instructions 

## Prerequisites 
   - Maven 3.3 
   - Java 8 
### Commands 
```
mvn clean install 
```
This will create and install the following modules in the local maven repository
#### Group:Artifact:Version 
com.apigee.api.commons:parent-pom:1.0.0-SNAPSHOT
com.apigee.archetype:api-pass-through:1.0.0-SNAPSHOT
    
# Generate Edge Proxy

Open a bash/cmd console . Run the below commands
Replace the below values 
@@helloWorld@@ - provide the API Name 
@@teamName@@ - Provide API team name ex : finance,Ops,marketing etc
``` 
mvn -B archetype:generate -DarchetypeGroupId=com.apigee.archetype \
-DgroupId=ignoreme -Dpackage=ignoreme -DarchetypeArtifactId=api-pass-through \
-DarchetypeVersion=1.0.0-SNAPSHOT -DartifactId=@@helloWorld@@ -DteamName=@@apollo@@ 
```
The edge proxy project is generated in the directory with the same name as the value of -DartifactId
